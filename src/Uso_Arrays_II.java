public class Uso_Arrays_II {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		String[] paises = new String[5];

		paises[0] = "Espa�a";
		paises[1] = "Mexico";
		paises[2] = "Argentina";
		paises[3] = "Colombia";
		paises[4] = "Chile";

		for (int i = 0; i < paises.length; i++) {

			System.out.println("Pais " + (i + 1) + " " + paises[i]);
		}

		for (String elemento : paises) {
			System.out.println(elemento);
		}

	}

}
