import java.util.Scanner;

public class Acceso_Aplicacion {

	public static void main(String[] args) {

		String clave_default = "Nestor";

		String clave_entrada = "";

		Scanner entrada = new Scanner(System.in);

		do {

			System.out.println("Escribe la clave de acceso");
			clave_entrada = entrada.nextLine();

			if (clave_entrada.equalsIgnoreCase(clave_default) == false) {
				System.out.println("Clave de acceso incorrecta");
			}

		} while (clave_default.equalsIgnoreCase(clave_entrada) == false);

		System.out.println("Acceso correcto");
	}

}
