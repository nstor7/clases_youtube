
public class Manipula_Cadenas_III {

	public static void main(String[] args) {

		String alumno1, alumno2;

		alumno1 = "Nestor";

		alumno2 = "nestor";

		System.out.println(alumno1.equals(alumno2));
		System.out.println(alumno1.equalsIgnoreCase(alumno2));
	}

}
