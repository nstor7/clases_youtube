package poo;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class Uso_Coche {

	public static void main(String[] args) throws IOException {

		Coche Seat = new Coche(); // INSTANCIAR UNA CLASE. SEAT ES UNA INSTANCIA DE LA CLASE COCHE

		// System.out.println("Este coche tiene " + Seat.ruedas + " ruedas");
		System.out.println(Seat.dimeLargo());

		BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
		System.out.println("�De que color es el coche?");
		String color = reader.readLine();
		Seat.establece_color(color);

		System.out.println(Seat.dimeColor());

		System.out.println(Seat.dimeDatosGenerales());

		System.out.println("�Tiene asientos de cuero?");
		String asientos = reader.readLine();
		Seat.configuraAsientos(asientos);

		System.out.println(Seat.dimeAsientos());

		System.out.println("�Tiene climatizador?");
		String climatizador = reader.readLine();
		Seat.tieneClimatizador(climatizador);

		System.out.println(Seat.dimeClimatizador());
	}

}
