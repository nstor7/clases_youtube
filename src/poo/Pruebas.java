package poo;

public class Pruebas {

	public static void main(String[] args) {

		Empleados empleado1 = new Empleados("Nestor");
		Empleados empleado2 = new Empleados("Maria");

		System.out.println(empleado1.devuelveDatos());

		empleado2.cambiaSeccion("Veterinaria");
		System.out.println(empleado2.devuelveDatos());
	}

}

class Empleados {

	private final String nombre;
	private String seccion;

	public Empleados(String nom) {

		nombre = nom;
		seccion = "Administración";

	}

	public void cambiaSeccion(String seccion) {
		this.seccion = seccion;
	}

	public String devuelveDatos() {
		return "El nombre es: " + nombre + " y la seccion es: " + seccion;
	}

}
