package poo;

import java.io.IOException;
import java.util.Date;
import java.util.GregorianCalendar;

public class Uso_Empleado {

	public static void main(String[] args) throws IOException {

		/*
		 * Empleado empleado1 = new Empleado("Nestor", 12000, 2018, 04, 17); Empleado
		 * empleado2 = new Empleado("Maria", 15000, 1995, 02, 21); Empleado empleado3 =
		 * new Empleado("Lorena", 25000, 2014, 01, 28);
		 * 
		 * empleado1.subeSueldo(5); empleado2.subeSueldo(5); empleado3.subeSueldo(5);
		 * 
		 * BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
		 * 
		 * System.out.println("Nombre : " + empleado1.getNombre() + " Sueldo: " +
		 * empleado1.getSueldo() + " Fecha de alta: " + empleado1.getAltaContrato());
		 * System.out.println("Nombre : " + empleado2.getNombre() + " Sueldo: " +
		 * empleado2.getSueldo() + " Fecha de alta: " + empleado2.getAltaContrato());
		 * System.out.println("Nombre : " + empleado3.getNombre() + " Sueldo: " +
		 * empleado3.getSueldo() + " Fecha de alta: " + empleado3.getAltaContrato());
		 */

		Empleado[] misEmpleados = new Empleado[3];

		misEmpleados[0] = new Empleado("Paco Gonz�lez", 24000, 2011, 01, 23);
		misEmpleados[1] = new Empleado("Laura L�pez", 14000, 2009, 04, 12);
		misEmpleados[2] = new Empleado("Raquel Garc�a", 24000, 2003, 12, 02);

		/*
		 * for (int i = 0; i < misEmpleados.length; i++) {
		 * 
		 * misEmpleados[i].subeSueldo(5); }
		 * 
		 * for (int i = 0; i < misEmpleados.length; i++) { System.out.println("Nombre: "
		 * + misEmpleados[i].getNombre() + " Sueldo: " + misEmpleados[i].getSueldo() +
		 * " Fecha de alta: " + misEmpleados[i].getAltaContrato()); }
		 */

		for (Empleado empleado : misEmpleados) {
			empleado.subeSueldo(5);
		}

		for (Empleado e : misEmpleados) {

			System.out.println("Nombre: " + e.getNombre() + " Sueldo: " + e.getSueldo() + " Fecha de alta: "
					+ e.getAltaContrato());
		}
	}

}

class Empleado {

	public Empleado(String nom, double sue, int agno, int mes, int dia) {

		nombre = nom;
		sueldo = sue;
		GregorianCalendar calendario = new GregorianCalendar(agno, mes - 1, dia);
		altaContrato = calendario.getTime();
	}

	public String getNombre() {
		return nombre;
	}

	public double getSueldo() {
		return sueldo;
	}

	public Date getAltaContrato() {
		return altaContrato;
	}

	public void subeSueldo(double porcentaje) {
		double aumento = sueldo * porcentaje / 100;
		sueldo = sueldo + aumento;
	}

	private String nombre;
	private double sueldo;
	private Date altaContrato;

}