package poo;

public class Coche {

	private int ruedas; // modificador de acceso, encapsulacion de la propiedad
						// no permite modificar el valor desde otra clase
	private int largo;
	private int ancho;
	private int motor;
	private int peso_plataforma;
	private String color;
	private int peso_total;
	private boolean asientos_cuero, climatizador;

	public Coche() {

		ruedas = 4;
		largo = 2000;
		ancho = 300;
		motor = 1600;
		peso_plataforma = 500;
	}

	public String dimeLargo() { // GETTER
		return "El largo del coche es " + largo;
	}

	public void establece_color(String color_coche) { // SETTER
		color = color_coche;
	}

	public String dimeColor() {
		return "El color es: " + color;
	}

	public String dimeDatosGenerales() {
		return "La plataforma del vehiculo tiene " + ruedas + " ruedas, " + ", Mide " + largo
				+ " metros con un ancho de " + ancho + "cm y un peso de " + peso_plataforma + " kg";
	}

	public void configuraAsientos(String asientos_cuero) {
		if (asientos_cuero.equalsIgnoreCase("si")) {
			this.asientos_cuero = true;
		} else {
			this.asientos_cuero = false;
		}
	}

	public String dimeAsientos() {

		if (asientos_cuero == true) {
			return "El coche tiene asientos de cuero";
		} else {
			return "El coche no tiene los asientos de cuero";
		}
	}

	public void tieneClimatizador(String climatizador) { // SETTER

		if (climatizador.equalsIgnoreCase("si")) {
			this.climatizador = true;
		} else {
			this.climatizador = false;
		}
	}

	public String dimeClimatizador() {

		if (climatizador == true) {
			return "El coche tiene climatizador";
		} else {
			return "El coche no tiene climatizador";
		}
	}

}
