import java.util.Scanner;

public class Switch_I {

	public static void main(String[] args) {

		Scanner entrada = new Scanner(System.in);

		System.out.println("Elige una opcion: \n1: Cuadrado \n2: Rectangulo \n3: Triangulo \n4: Circulo");

		int figura = entrada.nextInt();

		switch (figura) {

		case 1:

			System.out.println("Introduce el lado: ");
			int lado = entrada.nextInt();
			entrada.close();

			System.out.println("El area del cuadrado es: " + Math.pow(lado, 2));

			break;

		case 2:

			System.out.println("Introduce la base: ");
			int base = entrada.nextInt();

			System.out.println("Introduce la altura: ");
			int altura = entrada.nextInt();
			entrada.close();

			System.out.println("El area del rectangulo es: " + base * altura);

			break;

		case 3:

			System.out.println("Introduce la base: ");
			base = entrada.nextInt();

			System.out.println("Introduce la altura: ");
			altura = entrada.nextInt();
			entrada.close();

			System.out.println("El area del triangulo es: " + base * altura / 2);

			break;

		case 4:

			System.out.println("Introduce el radio: ");
			int radio = entrada.nextInt();
			entrada.close();

			System.out.print("El area del circulo es: ");

			System.out.printf("%1.2f", Math.PI * (Math.pow(radio, 2)));
			break;

		default:
			System.out.println("La opcion no es correcta");

		}

	}

}
