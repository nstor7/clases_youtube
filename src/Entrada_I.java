import java.util.Scanner;

public class Entrada_I {

	public static void main(String[] args) {

		String nombre;
		int edad;

		Scanner teclado = new Scanner(System.in);

		System.out.println("Introduce un nombre: ");

		nombre = teclado.nextLine();
		teclado.close();

		System.out.println("Introduce un numero: ");

		edad = teclado.nextInt();

		System.out.println("Has introducido el nombre: " + nombre + " y la edad: " + edad);

	}

}
