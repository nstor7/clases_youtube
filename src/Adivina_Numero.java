import java.util.Scanner;

public class Adivina_Numero {

	public static void main(String[] args) {

		int aleatorio = (int) (Math.random() * 100);

		int numero = 0;

		int intentos = 0;

		Scanner entrada = new Scanner(System.in);

		do {

			System.out.println("Introduce un numero");
			numero = entrada.nextInt();
			intentos++;

			if (numero < aleatorio) {

				System.out.println("Es mas alto");
			}

			else if (numero > aleatorio) {

				System.out.println("Es mas bajo");
			}

		} while (aleatorio != numero);

		System.out.println("Correcto! Lo has conseguido en " + intentos + " intentos");

	}

}
