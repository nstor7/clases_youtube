import java.util.Scanner;

public class Entrada_III {

	public static void main(String[] args) {

		Scanner entrada = new Scanner(System.in);

		System.out.println("Introduce un numero:");

		String num1 = entrada.nextLine();
		entrada.close();

		System.out.print("La raiz de " + num1 + " es: ");

		double num2 = Double.parseDouble(num1);
		System.out.printf("%1.2f", Math.sqrt(num2));

	}

}
