import javax.swing.JOptionPane;

public class Entrada_II {

	public static void main(String[] args) {

		String nombre = JOptionPane.showInputDialog("Introduce tu nombre");

		String edad = JOptionPane.showInputDialog("Introduce la edad");

		int edad_usuario = Integer.parseInt(edad);

		edad_usuario++;

		System.out.println("Hola " + nombre + ", el a�o que viene tendras " + edad_usuario + " a�os");
	}

}
